mk-configure (0.37.0-2) unstable; urgency=medium

  * Manually install by calling bmake directly.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 01 Jan 2023 16:13:04 +0100

mk-configure (0.37.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 28 Dec 2022 16:43:55 +0100

mk-configure (0.36.0-1) unstable; urgency=medium

  * New upstream release.
  * Port exists_make_target fixes from bmake.pm and makefile.pm.
  * Create a home directory and set MKCOMPILERSETTINGS=yes for mkc 0.36+.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 31 Jan 2021 18:25:20 +0100

mk-configure (0.35.really.0.33.0-1) unstable; urgency=medium

  * Revert to mk-configure 0.33.0 (Closes: #977908, #978271, #978387).

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 28 Dec 2020 20:05:56 +0100

mk-configure (0.35.0-1) unstable; urgency=medium

  * New upstream release.
  * Use secure URI in debian/watch.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 02 Dec 2020 15:37:12 +0100

mk-configure (0.33.0-3) unstable; urgency=medium

  * Wrap and sort control files.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.
  * Run tests but don’t fail.
  * Expand .PHONY.
  * Let dh_strip strip debug info for us.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 01 Mar 2020 15:35:14 +0100

mk-configure (0.33.0-2) unstable; urgency=medium

  * Add a basic autopkgtest.

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 21 Jan 2020 15:29:11 +0100

mk-configure (0.33.0-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/watch to also fetch tarballs directly from Git.
  * Drop no longer necessary patch, update README file name.
  * Use debhelper-compat instead of debian/compat.
  * d/control: Add Vcs-* field.
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Update debian/copyright.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 20 Jan 2020 13:51:40 +0100

mk-configure (0.29.1-3) unstable; urgency=medium

  [ Dima Kogan ]
  * Split .dot into two separate files (Closes: #895320).

 -- Andrej Shadura <andrewsh@debian.org>  Sat, 13 Apr 2019 17:37:58 +0200

mk-configure (0.29.1-2) unstable; urgency=medium

  * Add Homepage field.
  * Update the build dependency, since texlive-latex-recommended no
    longer provides latex-beamer (Closes: #867095).

 -- Andrew Shadura <andrewsh@debian.org>  Tue, 11 Jul 2017 11:53:01 -0500

mk-configure (0.29.1-1) unstable; urgency=medium

  * New upstream release.

 -- Andrew Shadura <andrewsh@debian.org>  Thu, 12 Nov 2015 19:25:52 +0100

mk-configure (0.29.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andrew Shadura <andrewsh@debian.org>  Sat, 25 Jul 2015 14:53:26 +0200

mk-configure (0.28.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop an old patch and a custom sys.mk.

 -- Andrew Shadura <andrewsh@debian.org>  Sun, 14 Sep 2014 17:41:43 +0200

mk-configure (0.27.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andrew Shadura <andrewsh@debian.org>  Fri, 22 Aug 2014 19:10:20 +0200

mk-configure (0.26.0-2) unstable; urgency=low

  * Fix pkg-config checks (Closes: #741837).

 -- Andrew Shadura <andrewsh@debian.org>  Sun, 16 Mar 2014 15:29:25 +0100

mk-configure (0.26.0-1) unstable; urgency=low

  * New upstream release.
  * Update watch file.
  * Ship presentation.pdf.
  * Recommend pkg-config.

 -- Andrew Shadura <andrewsh@debian.org>  Tue, 18 Feb 2014 22:05:01 +0100

mk-configure (0.25.0-1) unstable; urgency=low

  * New upstream release.
  * Drop old patches.
  * Use bmake's .ALLTARGETS feature to check for Makefile targets presence.

 -- Andrew Shadura <andrewsh@debian.org>  Sun, 05 Jan 2014 22:42:41 +0100

mk-configure (0.24.0-3) unstable; urgency=low

  * Fix the clean-up of MKC_SOURCE_FUNCLIBS.

 -- Andrew Shadura <andrewsh@debian.org>  Fri, 15 Nov 2013 17:39:37 +0100

mk-configure (0.24.0-2) unstable; urgency=low

  * Fix the Debhelper addon.

 -- Andrew Shadura <andrewsh@debian.org>  Fri, 15 Nov 2013 15:44:39 +0100

mk-configure (0.24.0-1) unstable; urgency=low

  * Initial release.

 -- Andrew Shadura <andrewsh@debian.org>  Thu, 15 Aug 2013 01:02:03 +0200
